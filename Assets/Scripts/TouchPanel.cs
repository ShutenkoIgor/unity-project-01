﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TouchPanel : MonoBehaviour, IDragHandler, IBeginDragHandler, IDropHandler
{
    private SphereMove sphere;

    #region MonoBehavior
    private void Awake()
    {
        sphere = FindObjectOfType<SphereMove>();
    }
    #endregion

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (GameManager.Instance().currentGameState != GameManager.enGameStates.GAME)
        {
            GameManager.Instance().onPlay.Invoke();
            return;
        }

        OnDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        sphere.onChangeVelocityX(eventData.delta.x);
    }

    public void OnDrop(PointerEventData eventData)
    {
        
    }
}
