﻿using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    [SerializeField] private int spawnCount = 0;

    [Space]
    [SerializeField] private float minDistanceBetween = 0;
    [SerializeField] private float maxDistanceBetween = 0;

    [Space]
    [SerializeField] private int maxPlatfoormOnArea = 0;

    [Space]
    [SerializeField] private GameObject platform = null;

    [Space]
    [SerializeField] private Transform parentPlatform = null;

    [Space]
    [SerializeField] private float movingOffset = 20f;
    [SerializeField] private float areaOffsetZ = 10f;

    [Space]
    [SerializeField] private int diamondChanse = 90;

    [Space]
    [SerializeField] private Collider edgeArea = null;

    private float playerDeltaX;
    private Vector3 spawnPos;
    private Transform playerTransform;

    private int curPlatformOnArea = 0;
    public int CurrentPlatformsSpawned
    {
        get
        {
            return curPlatformOnArea;
        }
        private set
        {
            curPlatformOnArea = value;
            CheckAreas();
        }
    }

    private List<Platform> platforms = new List<Platform>();

    #region MonoBehavior
    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        Spawn();
    }

    private void Update()
    {
        if (platforms[0].transform.position.z + movingOffset < playerTransform.position.z)
            MovePlatforms();
    }
    #endregion

    private void Spawn()
    {
        spawnPos = playerTransform.position;
        spawnPos.y -= 2f;
        playerDeltaX = spawnPos.x;

        Platform previousPlatform = null;

        for (int i = 0; i <= spawnCount; i++)
        {
            Platform tempPlatform = Instantiate(platform, spawnPos, 
                Quaternion.identity, parentPlatform).GetComponent<Platform>();

            platforms.Add(tempPlatform);
            SetNewPosition(tempPlatform.transform);

            if (i > 0)
            {
                previousPlatform.SetNextPosition(tempPlatform.transform);
            }

            if (i == spawnCount)
                tempPlatform.SetNextPosition(platforms[0].transform);

            previousPlatform = tempPlatform;
            CurrentPlatformsSpawned++;
        }
    }

    private void MovePlatforms()
    {
        Transform tempPlatform = platforms[0].transform;

        tempPlatform.position = spawnPos;

        platforms.Remove(platforms[0]);
        platforms.Add(tempPlatform.GetComponent<Platform>());
        SetNewPosition(tempPlatform);

        if (Random.Range(0,100) >= diamondChanse)
        {
            tempPlatform.Find("Diamond").gameObject.SetActive(true);
        }
        else
        {
            tempPlatform.Find("Diamond").gameObject.SetActive(false);
        }

        CurrentPlatformsSpawned++;
    }

    private void SetNewPosition(Transform _platform)
    {
        spawnPos.z += Random.Range(minDistanceBetween, maxDistanceBetween);
        spawnPos.x = Random.Range(edgeArea.bounds.min.x + _platform.localScale.x, edgeArea.bounds.max.x - _platform.localScale.x);
    }

    private void CheckAreas()
    {
        if(curPlatformOnArea >= maxPlatfoormOnArea)
        {
            curPlatformOnArea = 0;
            spawnPos.z += areaOffsetZ;
        }
    }
}
