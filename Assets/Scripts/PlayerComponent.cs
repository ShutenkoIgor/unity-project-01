﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class PlayerComponent : MonoBehaviour
{
    [SerializeField] private float minDistanceToFall = 0;
    public UnityEvent onPickUpDiamond { get; set; } = new UnityEvent();

    private Rigidbody rb;

    private int currentDiamondInGame = 0;

    #region MonoBehavior
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        EventsInit();
    }

    private void Update()
    {
        CheckDistance();
    }

    #endregion

    private void EventsInit()
    {
        onPickUpDiamond.AddListener(() => currentDiamondInGame++);

        GameManager.Instance().onGameOver.AddListener(() =>
        {
            UIManager.Instance().SetGameOverRewardText(currentDiamondInGame.ToString());
            rb.velocity = Vector3.zero;
        });

        GameManager.Instance().onWin.AddListener(() =>
        {
            UIManager.Instance().SetWinRewardText(currentDiamondInGame.ToString());
            rb.velocity = Vector3.zero;
            rb.constraints = RigidbodyConstraints.FreezeAll;
        });

        GameManager.Instance().onFinishGame.AddListener(() => GameManager.Instance().Crystal += currentDiamondInGame);
        GameManager.Instance().onPlay.AddListener(() => rb.useGravity = true);
    }

    private void CheckDistance()
    {
        if (transform.position.y >= minDistanceToFall || GameManager.Instance().currentGameState != GameManager.enGameStates.GAME) return;
        GameManager.Instance().onGameOver.Invoke();
    }
}
