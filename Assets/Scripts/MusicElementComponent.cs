﻿using UnityEngine.UI;
using UnityEngine;

public class MusicElementComponent : MonoBehaviour
{
    public Text clipName;
    public Button playButton;
    public GameObject playImg;
    public GameObject costCrystalImg;
}
