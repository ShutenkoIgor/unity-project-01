﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private GameObject gamePanel = null;
    [SerializeField] private GameObject gameOverPanel = null;
    [SerializeField] private GameObject winPanel = null;
    [SerializeField] private GameObject menuPanel = null;
    [SerializeField] private GameObject holdPanel = null;

    [Space]
    [SerializeField] private Text gameOverRewardText = null;
    [SerializeField] private Button gameOverMenuButton = null;

    [Space]
    [SerializeField] private Text winRewardText = null;
    [SerializeField] private Button winMenuButton = null;

    [Space]
    [SerializeField] private Image progressBar = null;

    [Space]
    [SerializeField] private Text crystalCountText = null;

    #region MonoBehavior
    private void Start()
    {
        ButtonsInit();
        EventsInit();
        CrystalUpdate();
    }

    private void Update()
    {
        ProgressUpdate();
    }
    #endregion

    private void ButtonsInit()
    {
        gameOverMenuButton.onClick.AddListener(GameManager.Instance().RestartGame);
        winMenuButton.onClick.AddListener(GameManager.Instance().RestartGame);
    }

    private void EventsInit()
    {
        GameManager.Instance().onPlay.AddListener(() =>
        {
            holdPanel.SetActive(false);
            ShowGamePanel();
        });

        GameManager.Instance().onGameOver.AddListener(() => ShowGameOverPanel());
        GameManager.Instance().onWin.AddListener(() => SwhowWinPanel());
    }

    #region Panels
    public void CloseAllPanels()
    {
        gamePanel.SetActive(false);
        gameOverPanel.SetActive(false);
        winPanel.SetActive(false);
        menuPanel.SetActive(false);
    }

    public void ShowGamePanel()
    {
        CloseAllPanels();
        gamePanel.SetActive(true);
    }

    public void ShowGameOverPanel()
    {
        CloseAllPanels();
        gameOverPanel.SetActive(true);
    }

    public void SwhowWinPanel()
    {
        CloseAllPanels();
        winPanel.SetActive(true);
    }

    public void ShowMenuPanel()
    {
        CloseAllPanels();
        menuPanel.SetActive(true);
    }

    public void ShowHoldPanel() => holdPanel.SetActive(true);

    #endregion

    public void SetGameOverRewardText(string _reward) => gameOverRewardText.text = _reward; 
    public void SetWinRewardText(string _reward) => winRewardText.text = _reward; 

    public void CrystalUpdate() => crystalCountText.text = GameManager.Instance().Crystal.ToString(); 

    public void ProgressUpdate()
    {
        if (GameManager.Instance().currentGameState != GameManager.enGameStates.GAME) return;

        float curProgress = 0;

        curProgress = AudioManager.Instance().GetMainAudioSource().time / AudioManager.Instance().GetMainAudioSource().clip.length;
        progressBar.fillAmount = curProgress;

        CheckWin(curProgress);
    }

    private void CheckWin(float _value)
    {
        if (!Mathf.Approximately(_value, 1) || GameManager.Instance().currentGameState != GameManager.enGameStates.GAME) return;
        GameManager.Instance().onWin.Invoke();
    }
}
