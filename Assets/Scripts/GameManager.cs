﻿using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public UnityEvent onGameOver { get; set; } = new UnityEvent();
    public UnityEvent onWin { get; set; } = new UnityEvent();
    public UnityEvent onPlay { get; set; } = new UnityEvent();
    public UnityEvent onFinishGame { get; set; } = new UnityEvent();

    private int crystal;
    public int Crystal
    {
        get => crystal;
        set
        {
            crystal = value;
            SaveManager.SetKey("Crystal",crystal);
            UIManager.Instance().CrystalUpdate();
        }
    }

    public enum enGameStates
    {
        MENU,
        GAME,
        GAME_OVER,
        WIN
    }
    public enGameStates currentGameState;

    private void Awake()
    {
        crystal = SaveManager.GetKey("Crystal",0);

        onGameOver.AddListener(() => currentGameState = enGameStates.GAME_OVER);
        onPlay.AddListener(() => Instance().currentGameState = enGameStates.GAME);
        onWin.AddListener(() => currentGameState = enGameStates.WIN);

    }

    public void RestartGame()
    {
        onFinishGame.Invoke();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}