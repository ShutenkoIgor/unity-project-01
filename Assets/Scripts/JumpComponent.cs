﻿using UnityEngine;
using UnityEngine.Events;

public class JumpComponent : MonoBehaviour
{
    public UnityAction<Vector3> onJumping { get; private set; }

    private Rigidbody rb = null;

    [SerializeField] private float maxHeight = 0;
    [SerializeField] private float gravity = 0;

    [SerializeField] private float yOffset = 1.5f;

    [Space]
    [SerializeField] private bool playAutomatic = false;

    #region MonoBehavior
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        Physics.gravity = gravity * Vector3.up;

        onJumping += (Vector3 _nextPos) =>
        {
            rb.velocity = CalculateVelocity(_nextPos);
        };
    }
    #endregion

    private Vector3 CalculateVelocity(Vector3 _targetPos)
    {
        Vector3 deltaY = new Vector3(0, _targetPos.y + yOffset, 0);
        Vector3 deltaXZ = new Vector3();

        if (playAutomatic)
            deltaXZ = new Vector3(_targetPos.x - rb.transform.position.x, 0, _targetPos.z - rb.transform.position.z);
        else
            deltaXZ = new Vector3(0, 0, _targetPos.z - rb.transform.position.z);

        Vector3 upVelocity = Mathf.Sqrt(-2 * gravity * maxHeight) * deltaY.normalized;
        Vector3 sideVelocity = deltaXZ / (Mathf.Sqrt(-2 * maxHeight / gravity) + Mathf.Sqrt(2 * -(maxHeight - deltaY.magnitude) / gravity));

        Vector3 resultVelocity = upVelocity + sideVelocity;

        return resultVelocity;
    }
}
