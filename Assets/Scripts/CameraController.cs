﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float smooth = 5f;
    [SerializeField] private float distance = 5f;

    [SerializeField] private Transform target = null;

    private void Update()
    {
        Look();
    }

    private void Look()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x,transform.position.y,target.position.z - distance),smooth * Time.deltaTime);
    }
}
