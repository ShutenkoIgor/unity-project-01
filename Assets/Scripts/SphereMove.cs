﻿using UnityEngine;
using UnityEngine.Events;

public class SphereMove : MonoBehaviour
{
    public UnityAction<float> onChangeVelocityX { get; private set; }

    [SerializeField] private float sideSpeed = 0;

    #region MonoBehavior
    private void Awake()
    {
        onChangeVelocityX += (_horizontal) =>
        {
            if (GameManager.Instance().currentGameState != GameManager.enGameStates.GAME) return;
            transform.position += new Vector3(_horizontal, 0, 0) * sideSpeed * Time.deltaTime;
        };
    }
    #endregion
}
