﻿using UnityEngine;

[CreateAssetMenu(menuName = "Elements/Music Element")]
public class MusicElement : ScriptableObject
{
    [SerializeField] private string audioName = null;
    [SerializeField] private AudioClip clip = null;
    [SerializeField] private int cost = 0;

    public string GetAudioName() => audioName;
    public AudioClip GetAudioClip() => clip;
    public int GetCost() => cost;
    public bool IsPurchased() => System.Convert.ToBoolean(SaveManager.GetKey(audioName, 0));
    public void SetPurchaseStat(bool _stat) => SaveManager.SetKey(audioName,System.Convert.ToInt32(_stat));
}
