﻿using UnityEngine;
using UnityEngine.Events;

public class Platform : MonoBehaviour
{
    private Transform nextPlatform;

    public UnityEvent OnPlatformEnter { get; set; } = new UnityEvent();

    [SerializeField] private Animator anim = null;

    #region MonoBehavior
    private void Awake()
    {
        OnPlatformEnter.AddListener(() => anim.SetTrigger("Show"));
    }
    #endregion

    public void SetNextPosition(Transform _nextTransform) => nextPlatform = _nextTransform;

    private void OnCollisionEnter(Collision _col)
    {
        if (_col.transform.CompareTag("Player"))
        {
            _col.transform.GetComponent<JumpComponent>().onJumping.Invoke(nextPlatform.position);
            OnPlatformEnter.Invoke();
        }
    }
}