﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diamond : MonoBehaviour
{
    private void OnTriggerEnter(Collider _col)
    {
        if (_col.CompareTag("Player"))
        {
            transform.parent.Find("Hits").
                GetChild(Random.Range(0, transform.parent.Find("Hits").childCount)).
                GetComponent<ParticleSystem>().Play();

            _col.GetComponent<PlayerComponent>().onPickUpDiamond.Invoke();

            gameObject.SetActive(false);
        }
    }
}
