﻿using System.Collections.Generic;
using UnityEngine;

public class MusicList : MonoBehaviour
{
    [SerializeField] private Transform content = null;
    [SerializeField] private GameObject musicElementPrefab = null;

    private List<MusicElement> musicElements = new List<MusicElement>();

    #region MonoBehavior
    private void Awake()
    {
        musicElements.AddRange(Resources.LoadAll<MusicElement>("Elements/Audio"));
    }

    private void Start()
    {
        CreateItems();
    }
    #endregion

    private void CreateItems()
    {
        for (int i = 0; i < musicElements.Count; i++)
        {
            int temp = i;
            MusicElementComponent item = Instantiate(musicElementPrefab, content.position, Quaternion.identity, content).GetComponent<MusicElementComponent>();
            item.clipName.text = musicElements[temp].GetAudioName();

            if (temp == 0 && !musicElements[temp].IsPurchased())
                musicElements[temp].SetPurchaseStat(true);

            if (musicElements[temp].IsPurchased())
            {
                item.costCrystalImg.SetActive(false);

                item.playButton.onClick.AddListener(()=>
                {
                    AudioManager.Instance().audioSource.clip = musicElements[temp].GetAudioClip();
                    UIManager.Instance().ShowGamePanel();
                    UIManager.Instance().ShowHoldPanel();
                });
            }
            else
            {
                item.playImg.SetActive(false);
                item.costCrystalImg.transform.Find("Cost").GetComponent<UnityEngine.UI.Text>().text = musicElements[temp].GetCost().ToString();
                if (GameManager.Instance().Crystal >= musicElements[temp].GetCost())
                {
                    item.playButton.onClick.AddListener(() =>
                    {
                        GameManager.Instance().Crystal -= musicElements[temp].GetCost();
                        musicElements[temp].SetPurchaseStat(true);
                        ItemsUpdate();
                    });
                }
                else
                {
                    item.playButton.interactable = false;
                }
            }
        }
    }

    private void ItemsUpdate()
    {
        DeleteItems();
        CreateItems();
    }

    private void DeleteItems()
    {
        for(int i = content.childCount - 1; i >= 0; i--)
        {
            Destroy(content.GetChild(i).gameObject);
        }
    }
}
