﻿using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    public AudioSource audioSource { get; private set; }

    public AudioSource GetMainAudioSource() => audioSource;

    #region MonoBehavior
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        GameManager.Instance().onGameOver.AddListener(() => audioSource.Stop());
        GameManager.Instance().onPlay.AddListener(() => audioSource.Play());
    }
    #endregion
}
